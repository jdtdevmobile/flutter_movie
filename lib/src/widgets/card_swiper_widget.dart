import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter/material.dart';
import 'package:peliculas/src/models/pelicula_model.dart';


class CardSwiper extends StatelessWidget {// Esta clase permitira reutilizar el codigo en caso de que necesite usar cada vez una lista de tarjetas
  
  final List<Pelicula> peliculas;  // Con esta lista dinamica obtendremos una lista que deba enviarse por parametro cada vez que se vaya a usar esta clase para crear una animacion de tarjetas

  CardSwiper({@required this.peliculas }); //El elemento required indica que siempre voy a pedir la lista de peliculas de  manera obligatoria 
  
  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;  //El media query permite obtener informacion de la pantalla del dsipositivo donde se desplegara la aplicacion con el fin de adaptar la imagen a esta 
    
    return Container(
        padding: EdgeInsets.only(top: 10.0),
        child: Swiper( //Widget deslizador para montar la animacion de las tarjetas una despues de la otra.
         layout: SwiperLayout.STACK,
          itemWidth: _screenSize.width * 0.7,
          itemHeight: _screenSize.height * 0.5,
          itemBuilder: (BuildContext context, int index){

            peliculas[index].uniqueId = '${peliculas[index].id}-tarjeta';
            
            return Hero(
                  tag: peliculas[index].uniqueId,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),//Genera los bordes redondeados
                     child: GestureDetector(
                        onTap:() => Navigator.pushNamed(context, 'detalle', arguments: peliculas[index]),
                        child: FadeInImage(
                        image: NetworkImage(peliculas[index].getPosterImg()),
                        placeholder: AssetImage('assets/img/no-image.jpg'),
                        fit: BoxFit.cover,
                        
                    ),
                    
                )
                //child: Image.network("http://via.placeholder.com/350x150",fit: BoxFit.cover)
              ),
            );  
            
            
          },
          itemCount: peliculas.length, //muestra el numero de tarjetas disponibles para mostrar
          //pagination: new SwiperPagination(), //permite ver los 3 puntos en la parte inferior de la tarjetas
          //control: new SwiperControl(), //Permite ver las flechas de siguiente o anterior en las tarjetas ubicadas a izquierda o derecha
        ),
    );
  }
}
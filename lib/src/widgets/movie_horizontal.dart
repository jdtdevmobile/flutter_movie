import 'package:flutter/material.dart';
import 'package:peliculas/src/models/pelicula_model.dart';

class MovieHorizontal extends StatelessWidget {

  final List<Pelicula> peliculas;
  final Function siguientePagina; // estas dos propiedades deben ser obligatorias para MovieHorozontal

  MovieHorizontal({@required this.peliculas, @required this.siguientePagina}); //Este Widget recibe el listado de las peliculas obligatoriamente y las mostrara de forma horizontal y se puede reutilizar donde se requiera.

final _pageController = new PageController(//Controla la forma en que se muestra las tarjetas.
          initialPage: 1,// Cuantas tarjetas muestra en su parte inicial
          viewportFraction: 0.3, //Es el tamaño que tendra cada tarjeta
);
  
  @override
  Widget build(BuildContext context) {
    
    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() { //Agregamos una escucha al controller y luego la condicion para saber si ya llegamos al final de la pantalla

      if(_pageController.position.pixels >= _pageController.position.maxScrollExtent - 200){//esta condicion define si ya llegamos al final de la pantalla 
        siguientePagina();
        //print('Cargar siguientes peliculas');// Esto fue solamente para verificar el metodo anterior
      }

    });



    return Container( //contendra todos los elementos de la vista horizontal
      height: _screenSize.height * 0.2, //tomara el 20% de la pantalla para renderizar la vista de peliculas
      
      child: PageView.builder( //Permite renderizar las tarjetas de forma horizontal es decir una lista deslizable
                                //se cambio el PageView por el PageView.builder para optimizar la aplicacion y que se renderice
                                //Las peliculas cada que sea necesario y no todas las disponibles de manera simultanea
        pageSnapping:false, //Cambia la manera en que se desliza las tarjetas
        controller: _pageController,   
      //children: _tarjetas(context), //Este widget contiene la sintaxis que le da forma a las las tarjetas de peliculas.
      itemCount: peliculas.length, //Permite saber y controlar el conjunto de las siguientes peliculas que podra ir renderizando
      itemBuilder: ( context, i){ //recibe el contexto y indice de la pelicula que debe renderizar.
        
          return _tarjeta(context, peliculas[i]); //Retorna el widget de la tarjeta que se va a renderizar y para esto recibe el contexto y el arreglo de las peliculas en el indice requerido
      } ,
    ),
  );
}

Widget _tarjeta(BuildContext context, Pelicula pelicula){ //Este widget me creara la pelicula en especifico que requerimos ir renderizando.
 pelicula.uniqueId = '${pelicula.id}-poster';
  //return Container( //Se cambia este return por la variable tarjeta para que el codigo a nivel de identacion y forma no quede muy complejo al agregar mas widgets

    final tarjeta = Container(   
        margin: EdgeInsets.only(right: 10.0),//separacion entre tarjetas 
        child: Column(
          children: <Widget> [
                Hero(
                  tag: pelicula.uniqueId,
                  child: ClipRRect( //permite dar forma a los bordes.
                    borderRadius: BorderRadius.circular(20.0), //Permite poner bordes redondeados de las tarjetas
                    child: FadeInImage(//Se envuelve en el clipRRect para darle forma a los bordes de la tarjetas
                   image: NetworkImage(pelicula.getPosterImg() ), //Permite cargar la imagen de la pelicula
                   placeholder: AssetImage('assets/img/no-image.jpg'),
                   fit: BoxFit.cover, //Permite tomar el ancho posible de las tarjetas
                   height: 132.0,
                ),
              ),
            ),
            SizedBox(height:5.0), //Da un espaciado entre la tarjeta y el nombre de la pelicula
            Text( pelicula.title,//Pone el nombre de la pelicula debajo de cada tarjeta 
              overflow: TextOverflow.ellipsis, //Agrega 3 puntos cuando el nombre de la pelicula es muy largo 
              //style: Theme.of(context).textTheme.caption,//pone el texto un poco mas pequeño
            )  
          ],
        ),
      );
      
    return GestureDetector(//Retorna un widget GestureDetector que permite detectar cuando se da click sobre un elemento
      child: tarjeta, //Lo aplica sobre la tarjeta
      onTap: (){
        print('Nombre de la pelicula: ${pelicula.title}'); //Imprime por consola en mnombre de la pelicula
        
        Navigator.pushNamed(context, 'detalle', arguments: pelicula);// Permite navegar a la otra pantalla
      },


    );  
}

  List <Widget> _tarjetas(BuildContext context) {// Este widget ya no se esta usando debido a que se creo el widget personalizado de tarjeta. ¿ 

    return peliculas.map( (pelicula) { //Esto retornara una lista que se transforma en un listado de widgets

      return Container(
        margin: EdgeInsets.only(right: 10.0),//separacion entre tarjetas 
        child: Column(
          children: <Widget> [
            ClipRRect( //permite dar forma a los bordes.
              borderRadius: BorderRadius.circular(20.0), //Permite poner bordes redondeados de las tarjetas
              child: FadeInImage(//Se envuelve en el clipRRect para darle forma a los bordes de la tarjetas
                 image: NetworkImage(pelicula.getPosterImg() ), //Permite cargar la imagen de la pelicula
                 placeholder: AssetImage('assets/img/no-image.jpg'),
                 fit: BoxFit.cover, //Permite tomar el ancho posible de las tarjetas
                 height: 132.0,
              ),
            ),
            SizedBox(height:5.0), //Da un espaciado entre la tarjeta y el nombre de la pelicula
            Text( pelicula.title,//Pone el nombre de la pelicula debajo de cada tarjeta 
              overflow: TextOverflow.ellipsis, //Agrega 3 puntos cuando el nombre de la pelicula es muy largo 
              //style: Theme.of(context).textTheme.caption,//pone el texto un poco mas pequeño
            )  
          ],
        ),
      );
    
    }).toList(); // Esto lo convierte en una lista de Widgets

  }
}
import 'package:flutter/material.dart';
import 'package:peliculas/src/providers/peliculas_provider.dart';
import 'package:peliculas/src/search/search_delegate.dart';

import 'package:peliculas/src/widgets/card_swiper_widget.dart';
import 'package:peliculas/src/widgets/movie_horizontal.dart';

class HomePage extends StatelessWidget {
  final peliculasProvider = PeliculasProvider(); // Esta linea estaba dentro del swiper y se movio a esta parte del codigo

  @override
  Widget build(BuildContext context) {

    peliculasProvider.getPopulares();// Esta linea se agrego despues del stream builder para mostrar de nuevo la lista horizontal


    return Scaffold(
        appBar: AppBar(
            title: Text('My flutter App1 Peliculas en cines'),
            backgroundColor: Colors.indigoAccent,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {

                  showSearch(
                    context: context, 
                    delegate: DataSearch(),
                    //query: 'Hola'
                    
                    );

                },
              )
            ]),
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              _swiperTarjetas(), //Muestra las peliculas con un swper o deslizador en la parte superior de la pantalla consumiendo el servicio desde la BD de peliculas y apuntando al endPoin play_now
              _footer(context), //Muestra las peliculas en el footer o pie de pagina de forma horizontal y apuntando al endPoint de populares
            ],
          ),
        )
    );
  }

  Widget _swiperTarjetas() {
    //Widget personalizado que contiene la logica para las tarjetas

    return FutureBuilder(
      future: peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        //snapchot contiene la lista de peliculas y es lo que resuelve el future

        if (snapshot.hasData) {
          //Valida si snapshot tiene informacion
          return CardSwiper(peliculas: snapshot.data); //si tiene informacion muestra la data de
        } else {
          //De lo contrario muestra un circulo inicador de progreso
          return Container(
              height: 400.0,
              child: Center(
                  child:CircularProgressIndicator() //Este indicador circular se muestra en la parte central inferior
              )
          );
        }
      },
    );

    // Aca se crea una instancia de PeliculasProvider lineas arriba y luego se llama el metodo getEnCines() y asi muestra la respuesta por consola
    //peliculasProvider.getEnCines();

    //return  CardSwiper(
    //peliculas: [1,2,3,4,5,6],
    //);
  }

  Widget _footer(BuildContext context) {
    //Widget personalizado (footer o pie de pagina) mostrara en un infinite scroll horizontal toda la informacion de las peliculas populares.

    return Container(
      width: double.infinity, //Infinity para que tome todo el ancho en la pantalla
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start, //pone la palabra populares al lado izquierdo
        children: <Widget>[
          Container(
              //se puso el texto dentro de un conainer para poder dar propiedad de espaciado a la palabra populares
              padding: EdgeInsets.only(left: 20.0), //mueve (populares un poco del borde izquierdo)
              child: Text('Populares', style: Theme.of(context).textTheme.headline6) //Pone el texto Populares
              ),
          SizedBox(height: 5.0), //Para darle espacio a la palabra populares

          //Es necesario cambiar este FutureBuilder, para poder implementar un Streambuilder
          //o flujo de informacion de (peliculas) es decir, que no se cargue unicamente la primera pagina de peliculas 
          //sino que cargue una a una las distintas paginas de peliculas que existe en la Base de datos de la pagina de peliculas)
          //Para esto hay que implementar en peliculas_provider el Stream de lista de pelicula 
          
          //FutureBuilder( se cambia este Future por un Stream builder. Hacer enfasis en la diferencia entre uno y otro ? 
            StreamBuilder(
            stream: peliculasProvider.popularesStream,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              //snapshot.data?.forEach( (p) => print(p.title));

              if (snapshot.hasData) {
                //Valida si hay peliculas
                return MovieHorizontal(
                  peliculas: snapshot.data,
                  siguientePagina: peliculasProvider.getPopulares, //Esta fue la ultima linea que implementa la carga de peliculas usando el stream
                  //Solamente se envia la definicion al metodo getpopulares sin () por esta razon no los tiene.
                  );
              } else {
                return Center(child: CircularProgressIndicator()); //Centra el loading
              }
            },
          ),
        ],
      ),
    );
  }
}

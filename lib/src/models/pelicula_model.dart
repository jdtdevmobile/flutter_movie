
//Esta clase Peliculas representa un contenedor de todas las pelicuals que se crearan.
class Peliculas {
  // ignore: deprecated_member_use
  List<Pelicula> items = new List(); //Contendra una colleccion de Pelicula

  Peliculas();

  Peliculas.fromJsonList( List<dynamic> jsonList ){ //Este contrusctor me permite recibir el mapa de todas las respuestas de las caracteristicas que contiene una pelicula.

    if( jsonList == null) return;//Se hace una validacion para verificar el contenido de la lista.

    for( var item in jsonList ){ //recorremos con la variable item la lista(jsonList) que contiene toda la informacion de una pelicula
      final pelicula = new Pelicula.fromJsonMap(item);// Creamos una nueva instancia de (pelicula) que contendra la informacion que se ira recopilando dato a dato en la variable item
      items.add( pelicula );// se agregara a la instancia o objeto (pelicula) uno uno cada cada caracteristica de la pelicula guardada en la variable item
    }
  }
}

//Esta clase representa el modelo que contiene los datos de una pelicula (Paso 1 )
class Pelicula {

  String uniqueId;

  int voteCount;
  int id;
  bool video;
  double voteAverage;
  String title;
  double popularity;
  String posterPath;
  String originalLanguage;
  String originalTitle;
  List<int> genreIds;
  String backdropPath;
  bool adult;
  String overview;
  String releaseDate;

//Asigno las caracteristicas de la pelicula a las propiedades en esta clase (paso 1)
  Pelicula({
    this.voteCount,
    this.id,
    this.video,
    this.voteAverage,
    this.title,
    this.popularity,
    this.posterPath,
    this.originalLanguage,
    this.originalTitle,
    this.genreIds,
    this.backdropPath,
    this.adult,
    this.overview,
    this.releaseDate,
  });

  //Este metodo crea una instancia de una pelicula que lee los datos de un mapa que esta en un formato json
  Pelicula.fromJsonMap( Map<String, dynamic> json ){ //Es un mapa con un argumento tipo  String y otro dynamic porque los datos que son consultados son de distinto tipo 
    //Ahora se toma cada valor del json y se asigna a cada propiedad en esta clase.
    voteCount        = json['vote_count'];
    id               = json['id'];
    video            = json['video'];
    voteAverage      = json['vote_average'] / 1; //Esta div se hace para garantizar que el valor sea un double 
    title            = json['title'];
    popularity       = json['popularity'] / 1;
    posterPath       = json['poster_path'];
    originalLanguage = json['original_language'];
    originalTitle    = json['original_title'];
    genreIds         = json['genre_ids'].cast<int>(); //Se garantiza que sea un entero
    backdropPath     = json['backdrop_path'];
    adult            = json['adult'];
    overview         = json['overview'];
    releaseDate      = json['release_date'];


  }

  getPosterImg(){ //Este metodo permite retornar todo el url de la imagen 

    if ( posterPath == null){
      return 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/600px-No_image_available.svg.png'; //esta imagen se carga cuando el poster path es null
    } else{
    return 'https://image.tmdb.org/t/p/w500/$posterPath';
    }

}
getBackgroundImg(){ //Este metodo permite retornar todo el url de la imagen 

    if ( posterPath == null){
      return 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/600px-No_image_available.svg.png'; //esta imagen se carga cuando el poster path es null
    }else{
    return 'https://image.tmdb.org/t/p/w500/$backdropPath';
    }
  }
}
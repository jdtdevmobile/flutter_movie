

import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';

import 'package:peliculas/src/models/pelicula_model.dart';
import 'package:peliculas/src/models/actores_model.dart';



//Clase proveedora de las peliculas es la forma en que la aplicacion va consumir el servicios de la Base de datos de peliculas (Puede ser definido como el archivo de conexion a la BD)
class PeliculasProvider {
//Elementos
  String _apikey    ='149ff4103bd49169c55a61622daaef36';
  String _url       = 'api.themoviedb.org';
  String _language  = 'es-ES';

  int _popularesPage = 0;// variable que controla el numero de la pagina de peliculas que se va a cargar.
  bool _cargando = false;
  //Stream que va a transmitir el listado de peliculas

  List<Pelicula> _populares =[]; //Esto es solo un contenedor de peliculas

  final _popularesStreamController = StreamController<List <Pelicula>>.broadcast(); //Se debe especificar que es lo que va a fluir
                                                                          //en este caso una List<Pelicula> y tambien se agrega
                                                                          //el broadcast para sea escuchado por varios (elmentos dentro de la app)
  
  //Se implementan 2 geters uno para insertar informacion de peliculas al stream y el otro para escuchar todo lo que el stream emita

  Function (List<Pelicula>) get popularesSink => _popularesStreamController.sink.add; //Estos geters deben ser una Funcion asi cumple con el tipo que requiere el stream

  //Ahora implemento el get para escuchar la informacion de las peliculas que se insertan en la linea anterior.

  Stream <List<Pelicula>> get popularesStream => _popularesStreamController.stream;

  void disposeStreams(){ //Todo Stream debe cerrarse para evitar multiples stream al entrar y salir de la pagina en la app 
  _popularesStreamController?.close(); // 
  }

  Future<List<Pelicula>> _procesarRespuesta(Uri url) async {

    final resp = await http.get(url);

    final decodedData = json.decode(resp.body);

    final peliculas = new Peliculas.fromJsonList(decodedData['results']);

    return peliculas.items;
  }

  

// final _popularesStreamController = new StreamController<List<Pelicula>>.broadcast();

//Function (List<Pelicula>) get popularesSink => _popularesStreamController.sink.add;

// Stream<List<Pelicula>> get popularesStream => _popularesStreamController.stream;

//Este metodo getEnCines() es el que  hace el llamado del EndPoint Movies now playing  de la pagina
Future<List<Pelicula>> getEnCines() async {
  //Debo de generar un url y para esto dart provee el elemento (Uri (objeto propio de dart))
                        //(authority, unencodePath) estos son los argumentos del (Uri) el segundo argumento es una ruta sin argumentos. 
  final url = Uri.https(_url, '3/movie/now_playing', {
    'api_key'       : _apikey,
    'language'      : _language
//El anterior metodo  hace la peticion al servicio y retorna las peliculas ya mapeadas y listas para ser usadas  
  });

  return await _procesarRespuesta(url);
//min 5:15 a que se refiere con el paquete de entrada.
//Despues de obtener el link se instala el flutter  packete http
//se pone en el pucspec.yaml y justo debajo de flutter_swiper pone http: ^0.12.0+2 y luego hace importacion del paquete
  
  
  //Ahora llama el metodo http.get y no se quiere almacenar el Future aqui y por eso se usa el await es decir que se espere hasta que haga la solicitud 
  //final resp = await http.get( url );
  
  //Ahora se decodifica la respuesta es decir la data (Aqui debo importatr la libreria dart conver)
  //final decodedData = json.decode( resp.body ); //en este body regresa toda la respuesta como un string y lo transforma en un mapa
  
  //Se Transforma el resultado en peliculas con este contructor que hace un barrido por cada una de los resultados desde jsonList que este en la lista y genera nuevas instancias de las peliculas de acuerdo al modelo y las almacena en (items que es la variable del ciclo for que esta en la clase peliculas donde se encuentra el modelo)  
  //final peliculas = new Peliculas.fromJsonList(decodedData['results']);

  //Aca se imprime una coleccion o lista de peliculas 
  //print ( peliculas.items[1].title );
  
  //Dicho mapa mencionado se imprime aca y para eso debe llamar a PeliculasProvider desde el home_page para poder imprimir la respuesta 
  //print( decordedData ['results'] );

  //return peliculas.items;
  //return [];

} 

  Future<List<Pelicula>> getPopulares() async {
  
  if(_cargando) return[];

     _cargando = true;
     
    _popularesPage++; //me incrementa en 1 el numero de la pagina

  

    print('Cargando sigueintes');

    final url = Uri.https(_url, '3/movie/popular', {
    'api_key'       : _apikey,
    'language'      : _language,
    'page'          : _popularesPage.toString()

  });

  final resp = await _procesarRespuesta(url);
//Asi estaba el codigo en su primera version con la sintaxis repetida para hacer la solicitud y consumir el servicio
  //final resp = await http.get( url );

  //final decodedData = json.decode( resp.body ); 

  //final peliculas = new Peliculas.fromJsonList(decodedData['results']);

  //return peliculas.items;

  _populares.addAll(resp);
  popularesSink( _populares );
  
  _cargando = false;
  return resp;



  }


//Peticion para obtener el recurso de los actores 

Future<List<Actor>> getCast( String peliId ) async {

  final url = Uri.https(_url, '3/movie/$peliId/credits',{
    'api_key'       : _apikey,
    'language'      : _language,

  });

  final resp = await http.get(url);
  final decodeData = json.decode( resp.body); //Toma el body y lo transforma en un mapa

  final cast = new Cast.fromJsonLis(decodeData['cast']);
  print(cast);

  return cast.actores;


}



Future<List<Pelicula>> buscarPelicula( String query ) async {

  final url = Uri.https(_url, '3/search/movie', {
    'api_key'  : _apikey,
    'language' : _language,
    'query'    : query
  });
  return await _procesarRespuesta(url);
 }
}



